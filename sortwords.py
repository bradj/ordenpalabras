#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    A= first.lower()
    B= second.lower()
    if len(A) > len(B):
        aux=A
        B=A
        aux=B
        for z in range (len(A)):
            lower= False
            if A[z] < B[z]:
                lower=True
                break
            return lower

def get_lower(words:list, pos:int):
    """Get lower word, for words right of pos (including pos)"""
    lower= pos

    for x in range(pos, len(words)):
       if  is_lower(words[pos], words[x])== False:
           pos=x
    return lower
def sort(words: list):
    """Return the list of words, ordered alphabetically"""
    list = []
    for x in range (0, len(words)):
        num=get_lower(words,x)
        list+= [words[num]]
        words[num]=words[x]
    return list

def show(words: list):
    """Show words on screen, using print()"""
    return print(words)

def main():
    words: list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)


if __name__ == '__main__':
    main()


